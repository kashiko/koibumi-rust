This crate is a minimal SOCKS5 client library.

The library is usable in async context.
The library is intended to be used with a local Tor SOCKS5 proxy.
