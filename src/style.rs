use iced::{button, Background, Color, Vector};

pub enum StartButton {
    Start,
    Stop,
    Abort,
}

impl button::StyleSheet for StartButton {
    fn active(&self) -> button::Style {
        button::Style {
            background: Some(Background::Color(match self {
                Self::Start => Color::from_rgb(0.11, 0.42, 0.87),
                Self::Stop => Color::from_rgb(0.8, 0.2, 0.2),
                Self::Abort => Color::from_rgb(0.5, 0.5, 0.5),
            })),
            border_radius: 8.0,
            shadow_offset: Vector::new(1.0, 1.0),
            text_color: Color::WHITE,
            ..button::Style::default()
        }
    }
}

pub struct TabButton {
    selected: bool,
}

impl TabButton {
    pub fn new(selected: bool) -> Self {
        Self { selected }
    }
}

impl button::StyleSheet for TabButton {
    fn active(&self) -> button::Style {
        if self.selected {
            button::Style {
                background: Some(Background::Color(Color::from_rgb(0.2, 0.2, 0.7))),
                border_radius: 4.0,
                text_color: Color::WHITE,
                ..button::Style::default()
            }
        } else {
            button::Style::default()
        }
    }
}

pub enum MessageEntryButton {
    Unread,
    Read,
    Selected,
}

impl button::StyleSheet for MessageEntryButton {
    fn active(&self) -> button::Style {
        match self {
            Self::Unread => button::Style {
                background: Some(Background::Color(Color::from_rgb(1.0, 1.0, 0.0))),
                ..button::Style::default()
            },
            Self::Read => button::Style::default(),
            Self::Selected => button::Style {
                background: Some(Background::Color(Color::from_rgb(0.0, 0.75, 0.0))),
                text_color: Color::WHITE,
                ..button::Style::default()
            },
        }
    }
}

pub enum SendButton {
    Enabled,
    Disabled,
}

impl button::StyleSheet for SendButton {
    fn active(&self) -> button::Style {
        button::Style {
            background: Some(Background::Color(match self {
                Self::Enabled => Color::from_rgb(0.9, 0.5, 0.3),
                Self::Disabled => Color::from_rgb(0.5, 0.5, 0.5),
            })),
            border_radius: 8.0,
            shadow_offset: Vector::new(1.0, 1.0),
            text_color: Color::WHITE,
            ..button::Style::default()
        }
    }
}
