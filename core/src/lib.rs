//! This crate is the core library for Koibumi, an experimental Bitmessage client.
//!
//! The library provides various types and methods usable to implement Bitmessage clients.
//!
//! See [`koibumi`](https://crates.io/crates/koibumi) or [`koibumi-sync`](https://crates.io/crates/koibumi-sync) for more about the application.
//! See [Bitmessage](https://bitmessage.org/) for more about the protocol.

#![deny(unsafe_code)]
#![warn(missing_docs)]

#[macro_use]
extern crate bitflags;
#[macro_use]
extern crate lazy_static;

pub mod address;
mod command;
mod config;
pub mod content;
pub mod crypto;
pub mod encoding;
mod error;
mod feature;
pub mod hash;
pub mod identity;
pub mod io;
pub mod message;
pub mod net;
mod net_addr;
pub mod object;
pub mod packet;
pub mod pow;
mod priv_util;
mod stream;
pub mod time;
pub mod var_type;

pub use config::{Builder as ConfigBuilder, Config};
