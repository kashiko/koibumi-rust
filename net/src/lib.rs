//! Types on networking including domain name and SOCKS address.

#![deny(unsafe_code)]
#![warn(missing_docs)]

pub mod domain;
mod port;
pub mod socks;

pub use crate::port::Port;
