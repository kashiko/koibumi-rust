use std::{collections::HashMap, convert::TryFrom, sync::Arc};

use crossbeam_channel::{select, Receiver};
use log::{debug, error};

use koibumi_core::{address::Address, identity::Private as PrivateIdentity, message, object};

use crate::{
    connection_loop::{Context, ShutdownCommand},
    manager::Event as BmEvent,
};

/// An object represents an user.
#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub struct User {
    id: Vec<u8>,
    subscriptions: Vec<Address>,
    private_identities: Vec<PrivateIdentity>,
}

impl User {
    /// Constructs a new user object from an ID and a subscription list.
    pub fn new(
        id: Vec<u8>,
        subscriptions: Vec<Address>,
        private_identities: Vec<PrivateIdentity>,
    ) -> Self {
        Self {
            id,
            subscriptions,
            private_identities,
        }
    }
}

#[derive(Debug)]
pub enum Event {
    User(User),
    //Getpubkey(message::Object),
    //Pubkey(message::Object),
    Msg(message::Object),
    Broadcast(message::Object),
    AddIdentity {
        id: Vec<u8>,
        identity: PrivateIdentity,
    },
    Subscribe {
        id: Vec<u8>,
        address: Address,
    },
}

pub fn manage(
    ctx: Arc<Context>,
    receiver: Receiver<Event>,
    shutdown_receiver: Receiver<ShutdownCommand>,
) {
    let bm_event_sender = ctx.bm_event_sender().clone();

    let mut users: HashMap<Vec<u8>, User> = HashMap::new();

    loop {
        select! {
            recv(receiver) -> event => match event {
                Ok(event) => {
                    match event {
                        Event::User(user) => {
                            users.insert(user.id.clone(), user);
                        }
                        Event::Msg(object) => match object::Msg::try_from(object.clone()) {
                            Ok(msg) => {
                                for user in users.values() {
                                    for identity in &user.private_identities {
                                        if let Err(_err) = msg.decrypt(object.header(), identity) {
                                            continue;
                                        }
                                        if let Err(_err) = bm_event_sender.send(BmEvent::Msg {
                                            user_id: user.id.clone(),
                                            address: identity.address().clone(),
                                            object: object.clone(),
                                        }) {
                                            continue;
                                        }
                                    }
                                }
                            }
                            Err(err) => {
                                debug!("{}", err);
                            }
                        },
                        Event::Broadcast(object) => match object::Broadcast::try_from(object.clone()) {
                            Ok(broadcast) => {
                                for user in users.values() {
                                    for subscription in &user.subscriptions {
                                        match &broadcast {
                                            object::Broadcast::V4(_) => {
                                                if let Err(_err) =
                                                    broadcast.decrypt(object.header(), &subscription)
                                                {
                                                    continue;
                                                }
                                            }
                                            object::Broadcast::V5(v5) => {
                                                if subscription.broadcast_tag() != *v5.tag() {
                                                    continue;
                                                }
                                            }
                                        }
                                        if let Err(err) = bm_event_sender.send(BmEvent::Broadcast {
                                            user_id: user.id.clone(),
                                            address: subscription.clone(),
                                            object: object.clone(),
                                        }) {
                                            error!("{}", err);
                                            continue;
                                        }
                                    }
                                }
                            }
                            Err(err) => {
                                debug!("{}", err);
                            }
                        },
                        Event::AddIdentity { id, identity } => {
                            if let Some(user) = users.get_mut(&id) {
                                if !user.private_identities.contains(&identity) {
                                    user.private_identities.push(identity);
                                }
                            }
                        }
                        Event::Subscribe { id, address } => {
                            if let Some(user) = users.get_mut(&id) {
                                if !user.subscriptions.contains(&address) {
                                    user.subscriptions.push(address);
                                }
                            }
                        }
                    }
                },
                Err(_err) => break,
            },
            recv(shutdown_receiver) -> _v => break,
        }
    }
}
