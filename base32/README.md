This crate is a Base32 encoder/decoder library.

The library is intended to be used to implement an Onion address encoder/decoder.
The library uses RFC 4648 Base32 alphabet, but encoded string is lowercase by default.
The library does not support padding.
