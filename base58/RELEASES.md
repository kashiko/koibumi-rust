# Version 0.0.1 (2021-01-)

* Added `deny(unsafe_code)` directive.

# Version 0.0.0 (2020-09-07)

* Features
    * Encodes byte array into Base58 string.
    * Decodes Base58 string into byte array.
