//! This crate is a Base58 encoder/decoder library.
//!
//! The library is intended to be used to implement a [Bitmessage](https://bitmessage.org/) address encoder/decoder.
//!
//! # Examples
//!
//! ```rust
//! use koibumi_base58 as base58;
//!
//! let test = base58::encode(b"hello");
//! let expected = "Cn8eVZg";
//! assert_eq!(test, expected);
//! # Ok::<(), Box<dyn std::error::Error>>(())
//! ```
//!
//! ```rust
//! use koibumi_base58 as base58;
//!
//! let test = base58::decode("Cn8eVZg")?;
//! let expected = b"hello";
//! assert_eq!(test, expected);
//! # Ok::<(), Box<dyn std::error::Error>>(())
//! ```

#![deny(unsafe_code)]
#![warn(missing_docs)]

#[macro_use]
extern crate lazy_static;

use std::{convert::TryInto, fmt};

use num_bigint::{BigUint, ToBigUint};
use num_integer::Integer;
use num_traits::Zero;

const ALPHABET: &[u8] = b"123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
const INVALID: u8 = ALPHABET.len() as u8;

lazy_static! {
    static ref ALPHABET_INDEX: [u8; 0x100] = {
        let mut index = [INVALID; 0x100];
        for i in 0..ALPHABET.len() {
            index[ALPHABET[i] as usize] = i as u8;
        }
        index
    };
}

/// Encodes byte array into Base58 string.
///
/// # Examples
///
/// ```rust
/// use koibumi_base58 as base58;
///
/// let test = base58::encode(b"hello");
/// let expected = "Cn8eVZg";
/// assert_eq!(test, expected);
/// # Ok::<(), Box<dyn std::error::Error>>(())
/// ```
pub fn encode(bytes: impl AsRef<[u8]>) -> String {
    let bytes = bytes.as_ref();
    if bytes.is_empty() {
        return String::with_capacity(0);
    }
    let mut n = BigUint::from_bytes_be(bytes);
    if n == Zero::zero() {
        return String::from_utf8(vec![ALPHABET[0]]).unwrap();
    }
    let mut list: Vec<u8> = Vec::new();
    let base = ALPHABET.len().to_biguint().unwrap();
    while n != Zero::zero() {
        let (q, r) = n.div_mod_floor(&base);
        list.push(r.try_into().unwrap());
        n = q;
    }
    let mut s = Vec::new();
    for i in list.iter().rev() {
        s.push(ALPHABET[*i as usize]);
    }
    String::from_utf8(s).unwrap()
}

#[test]
fn test_encode() {
    assert_eq!(encode(b""), "");
}

/// Indicates that an invalid Base58 character was found.
///
/// This error is used as the error type for the [`decode`] function.
///
/// [`decode`]: fn.decode.html
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct InvalidCharacter(char);

impl InvalidCharacter {
    /// Returns the actual character found invalid.
    pub fn char(&self) -> char {
        self.0
    }
}

impl fmt::Display for InvalidCharacter {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let ch = self.0;
        let code = u32::from(ch);
        if ch.is_control() {
            write!(f, "invalid character ({:#08x}) found", code)
        } else {
            write!(f, "invalid character '{}' ({:#08x}) found", ch, code)
        }
    }
}

impl std::error::Error for InvalidCharacter {}

fn to_num(ch: char) -> Result<u8, InvalidCharacter> {
    let i = ch as usize;
    if i > 0xff {
        return Err(InvalidCharacter(ch));
    }
    let v = ALPHABET_INDEX[i];
    if v == INVALID {
        Err(InvalidCharacter(ch))
    } else {
        Ok(v)
    }
}

/// Decodes Base58 string into byte array.
///
/// # Examples
///
/// ```rust
/// use koibumi_base58 as base58;
///
/// let test = base58::decode("Cn8eVZg")?;
/// let expected = b"hello";
/// assert_eq!(test, expected);
/// # Ok::<(), Box<dyn std::error::Error>>(())
/// ```
pub fn decode(s: impl AsRef<str>) -> Result<Vec<u8>, InvalidCharacter> {
    let s = s.as_ref();
    if s.is_empty() {
        return Ok(Vec::with_capacity(0));
    }
    let base = ALPHABET.len();
    let mut n: BigUint = Zero::zero();
    for c in s.chars() {
        n *= base;
        n += to_num(c)?;
    }
    Ok(n.to_bytes_be())
}

#[test]
fn test_decode() {
    assert_eq!(decode("").unwrap(), b"");
}
