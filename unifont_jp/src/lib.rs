//! GNU Unifont Glyphs - Japanese Version.
//!
//! The TTF file was retrieved from https://unifoundry.com/unifont/ .
//!
//! `koibumi_unifont_jp::TTF` is the reference to the TTF bytes.

#![deny(unsafe_code)]
#![warn(missing_docs)]

/// GNU Unifont Glyphs Japanese Version TTF data.
pub const TTF: &[u8] = include_bytes!("../include/unifont_jp-13.0.03.ttf");
